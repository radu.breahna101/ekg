import Vue from 'vue';
import VueCompositionAPI from '@vue/composition-api';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;
// global variable
Vue.prototype.$log = console.log;
new Vue({
  VueCompositionAPI,
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
