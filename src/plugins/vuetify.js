import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import RateQRS from '@/components/RateQRS.vue';
import PositiveQRS from '@/components/PositiveQRS.vue';
import IsoelectricQRS from '@/components/IsoelectricQRS.vue';
import NegativeQRS from '@/components/NegativeQRS.vue';
import DiphasicInitial from '@/components/DiphasicInitial.vue';
import DiphasicTerminal from '@/components/DiphasicTerminal.vue';
import LargeRwave from '@/components/LargeRwave.vue';
import RwaveDecrease from '@/components/RwaveDecrease.vue';
import SinV1 from '@/components/SinV1.vue';
import RinV5 from '@/components/RinV5.vue';
import InvertedT from '@/components/InvertedT.vue';
import InvertedTSymetric from '@/components/InvertedTSymetric.vue';
import STelevation from '@/components/STelevation.vue';
import STdepression from '@/components/STdepression.vue';
import SignificantQwave from '@/components/SignificantQwave.vue';
import WanderingP from '@/components/WanderingP.vue';
import AtrialFib from '@/components/AtrialFib.vue';
import AtrialEscapeBeat from '@/components/AtrialEscapeBeat.vue';
import JunctionalEscapeBeat from '@/components/JunctionalEscapeBeat.vue';
import VentricularEscapeBeat from '@/components/VentricularEscapeBeat.vue';
import PrematureAtrialBeat from '@/components/PrematureAtrialBeat.vue';
import PrematureJunctionalBeat from '@/components/PrematureJunctionalBeat.vue';
import PrematureVentricularBeat from '@/components/PrematureVentricularBeat.vue';
import SinusBlock from '@/components/SinusBlock.vue';
import AVBlock1 from '@/components/AVBlock1.vue';
import AVBlock2Mobitz21 from '@/components/AVBlock2Mobitz21.vue';
import AVBlock2Mobitz31 from '@/components/AVBlock2Mobitz31.vue';
import AVBlock2Wenckebach from '@/components/AVBlock2Wenckebach.vue';
import AVBlock3Complete from '@/components/AVBlock3Complete.vue';
import BBB from '@/components/BBB.vue';
import LeftBBB from '@/components/LeftBBB.vue';
import RightBBB from '@/components/RightBBB.vue';
import ECGLeads from '@/components/ECGLeads.vue';
import PwaveHeight from '@/components/PwaveHeight.vue';

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    values: {
      RateQRS: {
        component: RateQRS,
      },
      PositiveQRS: {
        component: PositiveQRS,
      },
      IsoelectricQRS: {
        component: IsoelectricQRS,
      },
      NegativeQRS: {
        component: NegativeQRS,
      },
      DiphasicInitial: {
        component: DiphasicInitial,
      },
      DiphasicTerminal: {
        component: DiphasicTerminal,
      },
      LargeRwave: {
        component: LargeRwave,
      },
      RwaveDecrease: {
        component: RwaveDecrease,
      },
      SinV1: {
        component: SinV1,
      },
      RinV5: {
        component: RinV5,
      },
      InvertedT: {
        component: InvertedT,
      },
      InvertedTSymetric: {
        component: InvertedTSymetric,
      },
      STelevation: {
        component: STelevation,
      },
      STdepression: {
        component: STdepression,
      },
      SignificantQwave: {
        component: SignificantQwave,
      },
      WanderingP: {
        component: WanderingP,
      },
      AtrialFib: {
        component: AtrialFib,
      },
      AtrialEscapeBeat: {
        component: AtrialEscapeBeat,
      },
      JunctionalEscapeBeat: {
        component: JunctionalEscapeBeat,
      },
      VentricularEscapeBeat: {
        component: VentricularEscapeBeat,
      },
      PrematureAtrialBeat: {
        component: PrematureAtrialBeat,
      },
      PrematureJunctionalBeat: {
        component: PrematureJunctionalBeat,
      },
      PrematureVentricularBeat: {
        component: PrematureVentricularBeat,
      },
      SinusBlock: {
        component: SinusBlock,
      },
      AVBlock1: {
        component: AVBlock1,
      },
      AVBlock2Mobitz21: {
        component: AVBlock2Mobitz21,
      },
      AVBlock2Mobitz31: {
        component: AVBlock2Mobitz31,
      },
      AVBlock2Wenckebach: {
        component: AVBlock2Wenckebach,
      },
      AVBlock3Complete: {
        component: AVBlock3Complete,
      },
      BBB: {
        component: BBB,
      },
      LeftBBB: {
        component: LeftBBB,
      },
      RightBBB: {
        component: RightBBB,
      },
      ECGLeads: {
        component: ECGLeads,
      },
      PwaveHeight: {
        component: PwaveHeight,
      },
    },
  },
});
