export const AXIS_UNDETERMINED = 'Undetermined'
export const AXIS_NORMAL = 'Axis within normal limits'
export const AXIS_LAD = 'Possible Left Axis Deviation'
export const AXIS_RAD = 'Right Axis Deviation'
export const AXIS_EXTREME = 'Extreme Axis'

export const QRS_NEGATIVE = 'Negative QRS'
export const QRS_POSITIVE = 'Positive QRS'
export const QRS_ISOELECTRIC = 'Isoelectric QRS'
