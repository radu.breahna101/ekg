import Vue from 'vue';
import Vuex from 'vuex';
import {
  AXIS_EXTREME,
  AXIS_LAD,
  AXIS_NORMAL, AXIS_RAD,
  AXIS_UNDETERMINED, QRS_ISOELECTRIC,
  QRS_NEGATIVE,
  QRS_POSITIVE,
} from '@/plugins/constants';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    currentStep: 0,
    maxSteps: 12,
    speed: 25,
    rate: 0,
    RRdistance: 1,
    PRdistance: 0,
    PRtime: 0,
    QRSinterval: 0,
    QRStime: 0,
    axisLeadI: null,
    axisLeadaVf: null,
    axisPosition: AXIS_UNDETERMINED,
    wanderingP: false,
    atrialFib: false,
    atrialEscapeBeat: false,
    junctionalEscapeBeat: false,
    ventricularEscapeBeat: false,
    prematureAtrialBeat: false,
    prematureJunctionalBeat: false,
    prematureVentricularBeat: false,
    sinusBlock: false,
    firstDegreeAVBlock: false,
    AVBlock2Wenckebach: false,
    AVBlock2Mobitz21: false,
    AVBlock2Mobitz31: false,
    AVBlock3Complete: false,
    BBB: false,
    RightBBB: false,
    LeftBBB: false,
    DiphasicInitial: false,
    DiphasicTerminal: false,
    PwaveHeight: false,
    LargeRwaveS1: false,
    RwaveDecrease: false,
    SwavePersists: false,
    SinV1: 0,
    RinV5: 0,
    InvertedT: false,
    InvertedTSymetric: false,
    STelevation: false,
    STdepression: false,
    SignificantQwave: false,
    conclusions: [],
  },
  mutations: {
    next(state) {
      if (state.currentStep < state.maxSteps) {
        state.currentStep += 1;
      }
    },
    previous(state) {
      if (state.currentStep > 0) {
        state.currentStep -= 1;
      }
    },

    setSpeed(state, speed) {
      state.speed = speed;
    },
    setRRdistance(state, RRdistance) {
      state.RRdistance = RRdistance;
    },
    setRate(state, rate) {
      state.rate = rate;
    },

    setCurrentStep(state, step) {
      state.currentStep = step;
    },
    setAxisLeadI(state, qrsType) {
      state.axisLeadI = qrsType;
    },
    setAxisLeadaVf(state, qrsType) {
      state.axisLeadaVf = qrsType;
    },
    setAxisPosition(state, position) {
      state.axisPosition = position;
    },
    setWanderingP(state, value) {
      state.wanderingP = value;
    },
    setAtrialFib(state, value) {
      state.atrialFib = value;
    },
    setAtrialEscapeBeat(state, value) {
      state.atrialEscapeBeat = value;
    },
    setConclusions(state, value) {
      state.conclusions = value;
    },
    setJunctionalEscapeBeat(state, value) {
      state.junctionalEscapeBeat = value;
    },
    setVentricularEscapeBeat(state, value) {
      state.ventricularEscapeBeat = value;
    },
    setPrematureAtrialBeat(state, value) {
      state.prematureAtrialBeat = value;
    },
    setPrematureJunctionalBeat(state, value) {
      state.prematureJunctionalBeat = value;
    },
    setPrematureVentricularBeat(state, value) {
      state.prematureVentricularBeat = value;
    },
    setSinusBlock(state, value) {
      state.sinusBlock = value;
    },
    setPRdistance(state, value) {
      state.PRdistance = value;
    },
    setPRtime(state, value) {
      state.PRtime = value;
    },
    setAVBlock2Wenckebach(state, value) {
      state.AVBlock2Wenckebach = value;
    },
    setAVBlock2Mobitz21(state, value) {
      state.AVBlock2Mobitz21 = value;
    },
    setAVBlock2Mobitz31(state, value) {
      state.AVBlock2Mobitz31 = value;
    },
    setBBB(state, value) {
      state.BBB = value;
    },
    setAVBlock3Complete(state, value) {
      state.AVBlock3Complete = value;
    },
    setRightBBB(state, value) {
      state.RightBBB = value;
    },
    setLeftBBB(state, value) {
      state.LeftBBB = value;
    },
    setQRSinterval(state, value) {
      state.QRSinterval = value;
    },
    setQRStime(state, value) {
      state.QRStime = value;
    },
    setDiphasicInitial(state, value) {
      state.DiphasicInitial = value;
    },
    setDiphasicTerminal(state, value) {
      state.DiphasicTerminal = value;
    },
    setPwaveHeight(state, value) {
      state.PwaveHeight = value;
    },
    setLargeRwaveS1(state, value) {
      state.LargeRwaveS1 = value;
    },
    setRwaveDecrease(state, value) {
      state.RwaveDecrease = value;
    },
    setSwavePersists(state, value) {
      state.SwavePersists = value;
    },
    setInvertedT(state, value) {
      state.InvertedT = value;
    },
    setSinV1(state, value) {
      state.SinV1 = value;
    },
    setRinV5(state, value) {
      state.RinV5 = value;
    },

    setInvertedTSymetric(state, value) {
      state.InvertedTSymetric = value;
    },
    setSTelevation(state, value) {
      state.STelevation = value;
    },
    setSTdepression(state, value) {
      state.STdepression = value;
    },
    setSignificantQwave(state, value) {
      state.SignificantQwave = value;
    },

  },
  actions: {
    setTimeActions({
      state,
      commit,
      dispatch,
    }) {
      dispatch('setPRtime');
      dispatch('setQRStime');
      dispatch('setGlobalConclusions');
    },
    setCalculatedRate({
      state,
      commit,
      dispatch,
    }) {
      const rate = 60 * (state.speed / state.RRdistance);
      commit('setRate', Math.round(rate));
      dispatch('setGlobalConclusions');
    },
    setPRtime({
      state,
      commit,
    }) {
      if ((state.speed === 0) || (state.PRdistance === 0)) {
        return;
      }
      const time = parseFloat(state.PRdistance / state.speed)
        .toFixed(2);
      commit('setPRtime', parseFloat(time));
    },
    setQRStime({
      state,
      commit,
    }) {
      if ((state.speed === 0) || (state.QRSinterval === 0)) {
        return;
      }
      const time = parseFloat(state.QRSinterval / state.speed)
        .toFixed(2);
      commit('setQRStime', parseFloat(time));
    },
    setCalculatedAxisPosition({
      state,
      commit,
    }) {
      const leadI = state.axisLeadI;
      const leadAVF = state.axisLeadaVf;
      let position = state.axisPosition;

      switch (true) {
        case (leadI === QRS_POSITIVE) && (leadAVF === QRS_POSITIVE):
          position = AXIS_NORMAL;
          commit('setAxisPosition', position);
          break;
        case (leadI === QRS_POSITIVE) && (leadAVF === QRS_NEGATIVE):
          position = AXIS_LAD;
          commit('setAxisPosition', position);
          break;
        case (leadI === QRS_NEGATIVE) && (leadAVF === QRS_POSITIVE):
          position = AXIS_RAD;
          commit('setAxisPosition', position);
          break;
        case (leadI === QRS_NEGATIVE) && (leadAVF === QRS_NEGATIVE):
          position = AXIS_EXTREME;
          commit('setAxisPosition', position);
          break;
        case (leadI === QRS_ISOELECTRIC) || (leadAVF === QRS_ISOELECTRIC):
          position = AXIS_UNDETERMINED;
          commit('setAxisPosition', position);
          break;
        default:
          commit('setAxisPosition', position);
      }
    },
    setGlobalConclusions({
      state,
      commit,
    }) {
      const wanderingP = {
        diagnosis: 'Wandering Pacemaker',
        motivation: 'A heartbeat less than 100 and an irregular P wave that changes shape',
        step: 'rhythmI',
      };
      const multifocal = {
        diagnosis: 'Multifocal Atrial Tachycardia',
        motivation: 'A heartbeat greater than 100 and an irregular P wave that changes shape',
        step: 'rhythmI',
      };
      const atrialFib = {
        diagnosis: 'Atrial Fibrillation',
        motivation: 'No discernible P waves and an irregular ventricular rhythm',
        step: 'rhythmI',
      };

      const atrialEscapeBeat = {
        diagnosis: 'Atrial Escape Beat',
        motivation: 'A pause between two cycles followed by a P wave that looks different to the others, the rhythm then goes back to normal',
        step: 'rhythmI',
      };

      const junctionalEscapeBeat = {
        diagnosis: 'Junctional Escape Beat',
        motivation: 'A normal QRS complex preceded by a pause and no P wave or an inverted P wave',
        step: 'rhythmII',
      };

      const ventricularEscapeBeat = {
        diagnosis: 'Ventricular Escape Beat',
        motivation: 'An enormous QRS complex preceded by a pause and no P wave',
        step: 'rhythmII',
      };

      const prematureAtrialBeat = {
        diagnosis: 'Premature Atrial Beat',
        motivation: 'P wave of unusual shape (looks different from the previous ones) which fires earlier than usual',
        step: 'rhythmII',
      };

      const prematureJunctionalBeat = {
        diagnosis: 'Premature Junctional Beat',
        motivation: 'Slightly widened premature QRS complex',
        step: 'rhythmIII',
      };

      const prematureVentricularBeat = {
        diagnosis: 'Premature Ventricular Beat',
        motivation: 'QRS complex is much wider taller and deeper than a normal QRS, following it is a pause before the next P-QRS',
        step: 'rhythmIII',
      };

      const sinusBlock = {
        diagnosis: 'Sinus Block',
        motivation: 'P wave and QRS complex is missing in a place where it should have been',
        step: 'rhythmIII',
      };
      const firstDegreeAVBlock = {
        diagnosis: 'First Degree Atrioventricular Block',
        motivation: 'PR interval is greater than 0.2 seconds',
        step: 'rhythmIV',
      };
      const AVBlock2Wenckebach = {
        diagnosis: 'Second Degree Atrioventricular Block Wenckebach',
        motivation: 'PR interval gradually lengthens until the QRS complex goes missing completely although the P wave that should have initiated it is still present, cycle repeats',
        step: 'rhythmIV',
      };
      const AVBlock2Mobitz21 = {
        diagnosis: 'Second Degree Atrioventricular Block Mobitz 2:1',
        motivation: '2 P waves for each 1 QRS complex',
        step: 'rhythmIV',
      };
      const AVBlock2Mobitz31 = {
        diagnosis: 'Second Degree Atrioventricular Block Mobitz 3:1',
        motivation: '3 P waves for each 1 QRS complex',
        step: 'rhythmIV',
      };
      const AVBlock3Complete = {
        diagnosis: 'Third Degree Atrioventricular Block (“complete”)',
        motivation: 'No P waves produce a QRS complex. The rhythm of the P waves and QRS complexes are independent of each other',
        step: 'rhythmV',
      };
      const BBB = {
        diagnosis: 'Bundle Branch Block',
        motivation: 'If the width of the QRS is equal or greater than 0.12 seconds and there are two specific R waves (not always evident) then there is cause to suspect a bundle branch block',
        step: 'rhythmV',
      };
      const RightBBB = {
        diagnosis: 'Right Bundle Branch Block',
        motivation: 'Two specific R waves in V1 or V2',
        step: 'rhythmV',
      };
      const LeftBBB = {
        diagnosis: 'Left Bundle Branch Block',
        motivation: 'Two specific R waves in V5 or V6',
        step: 'rhythmV',
      };
      const rightAtrialHypertrophy = {
        diagnosis: 'Right Atrial Hypertrophy',
        motivation: 'Large diphasic P wave with tall initial component or P wave height larger than 2.5 mm',
        step: 'hypertrophyI',
      };
      const leftAtrialHypertrophy = {
        diagnosis: 'Left Atrial Hypertrophy',
        motivation: 'Large diphasic P wave with wide terminal component',
        step: 'hypertrophyI',
      };

      const rightVentricularHypertrophy = {
        diagnosis: 'Right Ventricular Hypertrophy',
        motivation: 'R wave greater than S wave in V1; R wave progressively smaller from V2 to V4; S wave persists in V5 and V6; Right Axis Deviation',
        step: 'hypertrophyI',
      };

      const leftVentricularHypertrophy = {
        diagnosis: 'Left Ventricular Hypertrophy',
        motivation: 'S wave in V1 + R wave in V5 > 35 mm; Left Axis Deviation; Inverted T wave that slants downward gradually but up rapidly',
        step: 'hypertrophyII',
      };

      const ischemia = {
        diagnosis: 'Ischemia: reduced blood supply to the heart',
        motivation: 'Inverted and symmetrical T wave found in V1 to V6',
        step: 'infarction',
      };

      const myocardialInfarction = {
        diagnosis: 'Acute Myocardial Infarction',
        motivation: 'Elevation of the ST segment',
        step: 'infarction',
      };

      const subendocardialInfarction = {
        diagnosis: 'Subendocardial Infarction',
        motivation: 'Flat ST segment depression',
        step: 'infarction',
      };

      const necrosis = {
        diagnosis: 'Necrosis of a Myocardial Infarction',
        motivation: 'Significant Q wave with a width of at least 1mm + depth of approximately one third of the amplitude of the QRS complex',
        step: 'infarction',
      };

      const conclusion = [];

      if (state.rate <= 100 && state.wanderingP) {
        conclusion.push(wanderingP);
      }
      if (state.rate > 100 && state.wanderingP) {
        conclusion.push(multifocal);
      }
      if (state.atrialFib) {
        conclusion.push(atrialFib);
      }
      if (state.atrialEscapeBeat) {
        conclusion.push(atrialEscapeBeat);
      }

      if (state.junctionalEscapeBeat) {
        conclusion.push(junctionalEscapeBeat);
      }

      if (state.ventricularEscapeBeat) {
        conclusion.push(ventricularEscapeBeat);
      }

      if (state.prematureAtrialBeat) {
        conclusion.push(prematureAtrialBeat);
      }

      if (state.prematureJunctionalBeat) {
        conclusion.push(prematureJunctionalBeat);
      }
      if (state.prematureVentricularBeat) {
        conclusion.push(prematureVentricularBeat);
      }
      if (state.sinusBlock) {
        conclusion.push(sinusBlock);
      }

      if (state.PRtime > 0.2) {
        conclusion.push(firstDegreeAVBlock);
      }

      if (state.AVBlock2Wenckebach) {
        conclusion.push(AVBlock2Wenckebach);
      }

      if (state.AVBlock2Mobitz21) {
        conclusion.push(AVBlock2Mobitz21);
      }

      if (state.AVBlock2Mobitz31) {
        conclusion.push(AVBlock2Mobitz31);
      }
      if (state.AVBlock3Complete) {
        conclusion.push(AVBlock3Complete);
      }
      if (state.QRStime >= 0.12) {
        conclusion.push(BBB);
      }
      if (state.RightBBB) {
        conclusion.push(RightBBB);
      }
      if (state.LeftBBB) {
        conclusion.push(LeftBBB);
      }

      if (state.DiphasicInitial || state.PwaveHeight) {
        conclusion.push(rightAtrialHypertrophy);
      }
      if (state.DiphasicTerminal) {
        conclusion.push(leftAtrialHypertrophy);
      }

      if (state.LargeRwaveS1 && state.RwaveDecrease && state.SwavePersists && (state.axisPosition === AXIS_RAD)) {
        conclusion.push(rightVentricularHypertrophy);
      }

      if ((state.SinV1 + state.RinV5 > 35) && (state.axisPosition === AXIS_LAD) && state.InvertedT) {
        conclusion.push(leftVentricularHypertrophy);
      }

      if (state.InvertedTSymetric) {
        conclusion.push(ischemia);
      }
      if (state.STelevation) {
        conclusion.push(myocardialInfarction);
      }

      if (state.STdepression) {
        conclusion.push(subendocardialInfarction);
      }
      if (state.SignificantQwave) {
        conclusion.push(necrosis);
      }

      commit('setConclusions', conclusion);
    },
  },
  getters: {
    stepConclusions: (state) => (step) => state.conclusions.filter((conclusion) => conclusion.step === step),
  },
  modules: {},
});
