module.exports = {
  root: true,

  env: {
    node: true,
  },

  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],

  parserOptions: {
    parser: 'babel-eslint',
  },

  rules: {
    'no-console': 'off',
    'no-debugger': 'off',
    'vue/no-unused-components': 'warn',
    'vue/no-unused-vars': 'warn',
    'vue/max-len': 'off',
    'semi': 'off',
    'max-len': 'off',
    'no-unused-vars' : 'off',
  },

  'extends': [
    'plugin:vue/essential',
    '@vue/airbnb'
  ]
};
